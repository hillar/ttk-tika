#!/bin/bash

#set some params
#file to process
filename=$1
# tika server
tikaserver="localhost:9998"
# texta server
textaserver="localhost"
# texta import api fields
index="anyfile"
doc_type=${index}
# texta task
text_tagger_feature_names="X-TIKA:content"

# start ;)
# put to tika, get meta & text back
tikadata=$(curl -s -H "Accept: application/json" -T "${filename}"  http://${tikaserver}/rmeta/text| jq .[])

# add content md5
md5=$(cat "${filename}" | md5)
data=$(jq --argjson args "{\"content_md5\":\"${md5}\"}" '. += $args' <<< "${tikadata}")

# get texta API token
token=$(curl -s http://${textaserver}/account/get_auth_token -d '{"username":"admin","password":"1234"}' | jq .auth_token | sed 's/"//g')

# post it to texta import api
textaResult=$(curl -s -XPOST http://${textaserver}/import_api/document_insertion -d "{
    \"auth_token\": \"${token}\",
    \"index\": \"${index}\",
    \"doc_type\": \"${doc_type}\",
    \"data\": ${data}
}" | jq .message)

# if texta post was ok, do apply some preproccesing
# see https://texta-tk.github.io/api.html#apply-preprocessor-task
if [ "${textaResult}"  == "\"Item(s) successfully saved.\"" ];
then
    # ok to do some texta processing
    echo "TODO ..."
    exit
    dataset = ???
    curl -X POST http://${tikaserver}/task_manager/api/v1/apply -d '{
        \"auth_token\": \"${token}\",
        \"text_tagger_taggers\": [\"6\"],
        \"search\": \"2\",
        \"text_tagger_feature_names\": [\"${text_tagger_feature_names}\"],
        \"preprocessor_key\": \"text_tagger\",
        \"dataset\": ${dataset},
        \"description\": \"API-TASK\"
    }'
else
   echo "ERROR: $(date) ${filename} ${textaResult}"
fi
